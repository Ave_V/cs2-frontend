let addCourse = document.querySelector('#createCourse');

addCourse.addEventListener('submit', (e) => {
	//structure for when event is triggered

	e.preventDefault(); // this is to avoid page redirection

	let cName = document.querySelector('#courseName').value.toLowerCase()
	let cDesc = document.querySelector('#courseDescription').value
	let cPrice = document.querySelector('#coursePrice').value

	if(cName ==  "" || cDesc == "" || cPrice == "") {
		Swal.fire({
			icon: 'error',
			title: 'Hold on!',
			text: "It seems like you're not done with the form",
			footer: "Please make sure all fields are filled"
		})
	} else{
		fetch('https://serene-island-34692.herokuapp.com/api/courses/course-exists' || 'http://localhost:4000/api/courses/course-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: cName
			})
		}).then(res => res.json()).then(data => {
			console.log(data)
				if(data === false) {
					fetch('https://serene-island-34692.herokuapp.com/api/courses/addCourse' || 'http://localhost:4000/api/courses/addCourse', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							name: cName,
							description: cDesc,
							price: cPrice,

						})
					}).then(res => {
						return res.json()
					}).then(data => {
						console.log(data) 
						if(data === true) {
							Swal.fire({
								icon: "success",
								title: "Success!",
								text: "A new course has been added",
								timer: 2000,
								showConfirmButton: false
							})
						} else {
							Swal.fire(/*"Something went wrong. Please try again."*/{
								icon: "warning",
								title: "Uh Oh!",
								text: "Something went wrong",
								footer: "Please try again."
							})
						}
				})
			}  else {
				Swal.fire(/*'Course already exists. Please register a different one.'*/{
								icon: "error",
								title: "Uh Oh!",
								text: "Course already exists.",
								footer: "Please register a different one."
							})
			}
		})
	}
})