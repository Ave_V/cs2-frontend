let addUser = document.querySelector('#registerUser');

addUser.addEventListener('submit', (e) => {
	//structure for when event is triggered

	e.preventDefault(); // this is to avoid page redirection

	let fName = document.querySelector('#firstName').value
	let lName = document.querySelector('#lastName').value
	let uEmail = document.querySelector('#userEmail').value
	let mNo = document.querySelector('#mobileNumber').value
	let pWord1 = document.querySelector('#password1').value	
	let pWord2 = document.querySelector('#password2').value	

	if(pWord1 ==  "" && pWord2 == "" ) {
		Swal.fire(/*'Please input your password.'*/{
			icon: 'error',
			title: 'Oh no!',
			text: "It seems like you're not done with the form",
			footer: "Please make sure that all fields are filled."
		})
	} else if(pWord1 !== pWord2) {
		Swal.fire(/*'Passwords do not match. Please try again.'*/{
								icon: "warning",
								title: "Uh Oh!",
								text: "Passwords do not match.",
								footer: "Please try again."
							})
	} else if(mNo.length !== 11) {
		Swal.fire(/*'Mobile Number is invalid.'*/{
								icon: "warning",
								title: "Uh Oh!",
								text: "Mobile Number is invalid",
								footer: "Please try again."
							})
	} else{
		fetch('https://serene-island-34692.herokuapp.com/api/users/email-exists' || 'http://localhost:4000/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: uEmail
			})
		}).then(res => res.json()
		).then(data => {
			console.log(data)
			if(data === false) {
				/*ORIGINAL FETCH REQUEST*/
				fetch('https://serene-island-34692.herokuapp.com/api/users/register' || 'http://localhost:4000/api/users/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: fName,
						lastName: lName,
						email: uEmail,
						mobileNo: mNo,
						password: pWord1,
					})
				}).then(res => {
					return res.json()
				}).then(data => {
					console.log(data) 
					if(data === true) {
						Swal.fire(/*'New Account Registered successfully'*/{
								icon: "success",
								title: "Success!",
								text: "Your account has been registered!",
								timer: 2000,
								showConfirmButton: false
							})
					} else {
						Swal.fire(/*"Something went wrong. Please try again."*/{
								icon: "error",
								title: "Oh no!",
								text: "Something went wrong",
								footer: "Please try again."
							})
					}
				})
			} else {
				Swal.fire(/*'Email already exists. Please register a different one.'*/{
								icon: "error",
								title: "Oh no!",
								text: "Email already exists.",
								footer: "Please register a different one."
							})
			}
		})
	}
})
