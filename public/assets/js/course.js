const params = new URLSearchParams(window.location.search)
// window.location -> returns a location object with information about the "current" location of the document
// .search -> contains the query string section of the URL. returns an object of String data type
// query string is an object
// URLSearchParams() -> method/constructor that creates and returns a URLSearchParams object. it describes the interface that defines utility methods to work with the query string of a URL
// new -> instantiates a user-defined object

let id = params.get('courseId');
console.log(id);

const capitalize = (string) => {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

//capture the access token from local storage
let token = localStorage.getItem('token')
console.log(token);

let name = document.querySelector('#courseName')
let desc = document.querySelector('#courseDescription')
let price = document.querySelector('#coursePrice')

let enroll = document.querySelector('#enrollmentContainer')
let cDelete = document.querySelector('#enrollmentDelete')

fetch(`https://serene-island-34692.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data)

	name.innerHTML = capitalize(data.name)
	desc.innerHTML = data.description
	price.innerHTML = data.price

//insert the enroll button component in this page
enroll.innerHTML = `<a class="btn btn-success text-white btn-block" id="btnEnroll">Enroll</a><br>`
cDelete.innerHTML = `<a class="btn btn-danger text-white btn-block" id="btnDelete">Delete</a>`
})


//for enrolling the logged in student to the course

document.querySelector('#courseContainer').addEventListener('DOMContentLoaded', function () {
document.querySelector('#btnEnroll').addEventListener('click', (e) => {
	e.preventDefault();
	fetch(`http://localhost:4000/api/users/enroll`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`			
		},
		body: JSON.stringify({
			courseId: id
		})
	}).then(res => {
		return res.json()
		console.log(res)
	}).then(data => {
		console.log(data)
		if(data === true) {
			/*Swal.fire({
				icon: "success",
				title: "Success!",
				text: "You're now enrolled!"
			})*/
			alert('You have enrolled successfully!')
			window.location.replace("./course.html")
		} else {
			/*Swal.fire({
				icon: "warning",
				title: "Uh Oh!",
				text: "Something went wrong",
				footer: "Please try again."
			})*/
			alert('Something went wrong')
		}
	})
})
})

document.querySelector('#btnDelete').addEventListener('click', (e) => {
	e.preventDefault();

	window.location.replace("./deleteCourse.html")
})