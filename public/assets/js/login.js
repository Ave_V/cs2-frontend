let loginForm = document.querySelector('#loginUser')


loginForm.addEventListener("submit", (e) => {
   e.preventDefault()

   let email = document.querySelector("#userEmail").value
   console.log(email)
   let password = document.querySelector("#password").value 
   console.log(password) 

   //how can we inform a user that a blank input field cannot be logged in?
   if(email == "" || password == ""){
      Swal.fire(/*"Please input your email/password."*/{
      icon: 'error',
      title: 'Oh no!',
      text: "It seems like you're not done with the form",
      footer: "Please input your email/password."
    })
   }else{
      fetch(['https://serene-island-34692.herokuapp.com/api/users/login' || 'http://localhost:4000/api/users/login'], {
      	method: 'POST',
      	headers: {
      		'Content-Type': 'application/json'
      	},
      	body: JSON.stringify({
      		email: email,
      		password: password
      	})
      }).then(res => {
      	return res.json()
      }).then(data => {
      	console.log(data.access)
      	if(data.access){
           //lets save the access token inside our local storage.
           localStorage.setItem('token', data.access)
           //this local storage can be found inside the subfolder of the appData of the local file of the google chrome browser inside the data module of the user folder.
           Swal.fire(/*"access key saved on local storage."*/{
                icon: "success",
                title: "Success!",
                text: "Access key saved on local storage",
                timer: 2000,
                showConfirmButton: false
              }) //for educational purposes only. 
           window.location.replace("./courses.html")
           fetch([`https://serene-island-34692.herokuapp.com/api/users/details` || `http://localhost:4000/api/users/details`], {
           	headers: {
           		'Authorization': `Bearer ${data.access}`
           	}
           }).then(res => {
           	  return res.json()
           }).then(data => {
           	  localStorage.setItem("id", data._id)
           	  localStorage.setItem("isAdmin", data.isAdmin)
           	  console.log("items are set inside the local storage.")
           })
      	} else {
           //if there is no exixting access key value from the data variable then just inform the user.
           Swal.fire(/*"Something went wrong, Check your credentials"*/{
                icon: "error",
                title: "Oh no!",
                text: "Something went wrong.",
                footer: "Please check your credentials."
              })
      	}
      })
   }
})