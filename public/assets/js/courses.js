let container = document.querySelector('#coursesContainer');

fetch('https://serene-island-34692.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
	console.log(data);
	let courseData;

	if(data.length < 1) {
		courseData = 'No Course Available'
	} else {
		courseData = data.map(course => {
			console.log(course._id)
			return (
				`<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.name.toUpperCase()}</h5>
							<p class="card-text text-left">${course.description}</p>
							<p class="card-text text-left">P${course.price}</p>
							<p class="card-text text-left">${course.createdOn}</p>
						</div>
						<div class="card-footer">
							<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View Course Details</a>
						</div>
					</div>
				</div>` // ? -> inside the URL acts as a separator, it indicates the end of a URL RESOURCE PATH and indicates the start of the QUERY STRING
				// # -> originally used to jump to a specific element with the same id name/value
			)
		}).join("") //.join() is used to create a return of a new string. it concatenated all the objects inside the array and converted each item into a new string data type.
	}
	container.innerHTML = courseData;
})